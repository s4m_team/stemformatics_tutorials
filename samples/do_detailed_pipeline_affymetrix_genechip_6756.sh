#!/bin/bash
cd /data/datasets # edit path where necessary
s4m.sh -i 6756
cd 6756
ls
s4m.sh -s .

# get raw data
/home/tyronec/stemformatics_utils/get_data.sh . GSE29949 # edit path where necessary

# ill expand further on what get_data.sh does
# we can replicate the above command by these
####
# getting raw data
cd source/raw
wget 'http://www.ncbi.nlm.nih.gov/geo/download/?acc=GSE29949&format=file' -O 'GSE29949_RAW.tar'
tar -xvf 
gzip -dv *gz

# make sure to rename the files to reflect their GSM identity only
# eg GSM741192.CEL
# in some cases we get files named GSM741192_brain_microglia-1.CEL

# getting sample information
wget 'http://www.ncbi.nlm.nih.gov/geo/browse/?view=samples&series=GSE29949&zsort=date&mode=tsv&page=undefined&display=5000' -O 'GSE29949_sample_info.tsv'

# extracting sample information
cut -f1,2 "${_DIR}/${_GEO_PREFIX}${_GEO_ID}_sample_info.tsv" |\
    tail -n +2 |\
    sed "s|\"||g" > raw_sample_pheno.txt
cut -f1 raw_sample_pheno.txt > samples.txt

# we cut the first two columns
# by convention the first one is a GSM id
# and the second one is a sample description
# the sample descriptions are rarely informative by themselves
# so we have to format them appropriately
# note that bioreps should be named the same
# eg
# brain microglia 1
# brain microglia 2
# should all be converted to
# brain_microglia
# this will be noted by pipeline scripts and all samples will be coloured the same

####

# make sample phenotype map
sed -r -e "s|\-[0-9]||" -e "s|\-|_neg|" -e "s|\+|_pos|" raw_sample_pheno.txt > sample_pheno.txt
cut -f1 sample_pheno.txt > samples.txt

# paste the contents of samples.txt in the "Initialise biosamples metadata and METASTORE" tab
# grab metadata after init with chip id
wget "http://agile.stemformatics.org/agile_org/files/dataset/${PWD##*/}/METASTORE" -O METASTORE
wget "http://agile.stemformatics.org/agile_org/files/dataset/${PWD##*/}/biosamples_metadata.txt" -O biosamples_metadata.txt

s4m.sh -n . && s4m.sh -f . -C w3-s4m.conf -R /opt/R/bin/R && s4m.sh -v .  # -C flag should be set to current production server
s4m.sh -dp .  # make higher resolution plots to narrow down detection threshold
s4m.sh -d 3.6  .  # set detection threshold
# ive commented out the loading part
#s4m.sh --load . -C w3-s4m.conf
