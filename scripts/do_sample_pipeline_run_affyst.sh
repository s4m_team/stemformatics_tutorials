#!/bin/sh
# this script just runs the pipeline on sample datasets
DATASET_DIRECTORY_AFFY_ST=/data/datasets/test/template_6447_ready_to_normalise
INFILE_DIR=/tmp/datasets/template_6447_ready_to_normalise ## replace with a directory of user’s choice
cp -fR ${DATASET_DIRECTORY_AFFY_ST} ${INFILE_DIR}
cd ${INFILE_DIR}


red=`tput  setaf 1`
green=`tput  setaf 2`
reset=`tput sgr0`
echo "${red}"
echo "This is the start of the tutorial where the files are already available."
echo "Normally, the skeleton of the directory is created via the command : s4m.sh -i <ds_id> where <ds_id> is the dataset id."
echo "We don't need to do that here as all the files are already available."
echo "The files that need to be manually created are biosamples_metadata.txt, METASTORE and sample_pheno.txt."
echo "The data from GEO is in source/raw and are in the form of .CEL files"
echo "The name of the .CEL files is the name of the sample id. These sample ids are used in biosamples_metadata.txt and sample_pheno.txt"
echo "${red}"
echo "Press enter to continue"

read

echo ""
echo "ls of the base directory."
echo "${reset}"
ls
echo "${red}"
echo "ls of the source/raw directory"
echo "${reset}"
ls source/raw
echo "${red}"
echo "Press enter to continue"

read

echo "${red}"
echo "Showing the contents of METASTORE. The contents are propagated to the web server upon dataset upload."
echo "${reset}"

cat METASTORE

echo "${red}"
echo "Showing partial contents of biosamples_metadata.txt. The contents are propagated to the web server upon dataset upload."
echo "Press enter to continue"
echo "${reset}"

read

head -n24 biosamples_metadata.txt

echo "${red}"
echo "Showing sample_pheno.txt file. The first column is the sample_id , the second column "
echo "is the group that you want it to be in for the QC plots later on."
echo "${reset}"

cat sample_pheno.txt

echo "${red}"
echo "Press enter to continue"

read

echo "${red}"
echo "Running this command outputs the status of the dataset: s4m.sh -s ."
echo "${reset}"
s4m.sh -s .
echo "${red}"
echo "Press enter to continue"

read

echo "${red}"
echo “in this example the raw data, metadata and sample annotations are provided, we can proceed straight to normalisation”
echo “this will output a series of qc plots and bundle them into a pdf”
echo “regarding the APT flag, in the particular case of datasets associated with Affymetrix-ST platforms we manually specify the path to third party library used in the processing”
echo "s4m.sh -n . -APT /opt/APT/bin "
echo "Press enter to continue"
echo "${reset}"

read

s4m.sh -n . -APT /opt/APT/bin 

echo "${red}"
echo “after normalization, we generate gct files. change the server config to match the current production server.”
echo “we specify the server config file as we need to pull some data from Stemformatics in order for the finalisation to proceed”
echo “and we specify the path to a distinct version of R required by a specific script used in the finalisation process”
echo "Press enter to continue"
echo "${reset}"

read

s4m.sh -f . -C w3-s4m.conf -R /opt/R/bin/R

echo "${red}"
echo “before upload to Stemformatics, we validate the dataset”
echo "Press enter to continue"
echo "${reset}"

read

s4m.sh -v .

echo "${red}"
echo “in this example there are no visible errors, we can now upload”
echo “since this is an example we won’t actually load anything, but it can be done by running the following command:”
echo “s4m.sh --load . -C w3-s4m.conf”
echo "Remember to change the server config to match the current production server."
echo "${reset}
