#!/bin/sh
# TODO rework
# takes data directory, accession id, repository id. returns raw data, sample-data mappings
__author__="Tyrone Chen"
__copyright__=""
__credits__=["Tyrone Chen, Othmar Korn, Rowland Mosbergen, Christine Wells"]
__license__="GPL"
__version__="0.4.5"
__maintainer__="Tyrone Chen"
__email__="t.chen@uq.edu.au"
__status__="Development"

# FIXME NOT UNIT TESTED
# TODO partition SAMPLE INFO from RAW DATA, this is actually a 2-in-1 module
# TODO wire this directly into metastore later on
# TODO handle illumina data download (tricky to implement correctly)

# we set <dataset id> as directory
_DIR=$1
_DATA_DIR="${_DIR}/source/raw"

# accession id
_ACCESSION_ID=$2

# hardcoded parameters for accessing repositories
_GEO_INFO_URL_PREFIX='http://www.ncbi.nlm.nih.gov/geo/browse/?view=samples&series='
_GEO_INFO_URL_SUFFIX='&zsort=date&mode=tsv&page=undefined&display=5000'
_GEO_DATA_URL='ftp://ftp.ncbi.nlm.nih.gov/geo/series/'

_AE_META_URL='http://www.ebi.ac.uk/arrayexpress/xml/v2/experiments?accession='
_AE_INFO_URL='http://www.ebi.ac.uk/arrayexpress/files/'
_AE_DATA_URL='http://www.ebi.ac.uk/arrayexpress/xml/files/'

# SAMPLE INFO PROCESSING (GEO)
# download the sample and data relationship
get_sample_info_geo() {
    echo "Downloading sample and data relationships to ${_DIR}"
    wget -O "${_DIR}/${_GEO_PREFIX}${_GEO_ID}_sample_info.tsv" \
    "${_GEO_INFO_URL_PREFIX}${_GEO_ID}${_GEO_INFO_URL_SUFFIX}"
}

# extract sample-data relationship
extract_sample_info_geo() {
    echo "Extracting sample and data relationships"
    cut -f1,2 "${_DIR}/${_GEO_PREFIX}${_GEO_ID}_sample_info.tsv" |\
    tail -n +2 |\
    sed "s|\"||g" > raw_sample_pheno.txt
    cut -f1 raw_sample_pheno.txt > samples.txt
    cat samples.txt
}

# SAMPLE INFO PROCESSING (AE)
# extract sample-data relationship
extract_sample_info_ae () {
    echo "Extracting sample and data relationships"
    # get data from 1st two columns, remove header, order, purify GSM ids
    cut -f1,2,3 "${_DATA_DIR}/${_AE_PREFIX}${_AE_ID}.sdrf.txt" |\
    tail -n +2 |\
    sort |\
    sed "s|\s.*\t|\t|" > raw_sample_pheno.txt
    cut -f1 raw_sample_pheno.txt > samples.txt
    cat samples.txt
}

# RAW DATA PROCESSING (GEO)
# download the raw data
get_raw_data_geo () {
    echo "Downloading raw data to ${_DATA_DIR}"
    cd "${_DATA_DIR}" && \
    wget "${_GEO_DATA_URL}${_GEO_PREFIX}${_GEO_ID%???}nnn/${_GEO_PREFIX}${_GEO_ID}/suppl/*" && \
    cd ../..
}

# decompress data
inflate_raw_data_geo () {
    echo "Decompressing data"
    tar -xvf "${_DATA_DIR}"/*.tar -C "${_DATA_DIR}"
    gzip -dv "${_DATA_DIR}"/*.gz
}

# RAW DATA PROCESSING (AE)
# get the file list
get_file_list_ae () {
    echo "Downloading file list..."
    infile_path_list='file_list.txt'
    infile_path_info='file_info.txt'
    
    cd "${_DIR}/source/raw" && \
    wget -O "${infile_path_list}" "${_AE_DATA_URL}${_AE_PREFIX}${_AE_ID}" && \
    wget -O "${infile_path_info}" "${_AE_INFO_URL}${_AE_PREFIX}${_AE_ID}" &&
    cd ../..
}

# download the raw data
get_raw_data_ae () {
    echo "Downloading raw data to ${_DATA_DIR}"
    cd "${_DATA_DIR}" && \
    files=`xmllint file_list.txt --format |\
           grep "url" |\
           sed -r -e "s|.*<url>(.*)</url>.*|\1|"` && \
    for file in ${files};
    do
        wget "${file}"
    done && \
    cd ../..
}

# decompress data
inflate_raw_data_ae () {
    echo "Decompressing data"
    unzip "${_DATA_DIR}/"*.zip -d "${_DATA_DIR}"
}

# identify file formats (name check only)
identify_data_format () {
    echo "Checking filenames"
    # we support these formats only
    formats=(.gz .GZ .cel .CEL .txt .TXT .chp .CHP .bgx)
    
    # get a list of all formats
    format_exists=()
    
    for format in "${formats[@]}"; do
        if [ $(ls ${_DATA_DIR} | grep "${format}" | wc -l) != 0 ]; then
            format_exists+=("${format}")
            echo "Found ${format_exists[@]} files in ${_DATA_DIR}"
        fi
    done
    
    for files in ${format_exists[@]}
    do
        _rename_files "${files}"
    done
}

# cleanup filenames, ensure that directory specified in cmd args is correct
_rename_files () {
    for file in ${_DATA_DIR}/*$1; do 
        # we slice at the _ delimiter because it is the most common scenario
        mv "$file" "${file/_*/$1}"
        echo ${file/_*/$1}
    done
}

# identify the repository by parsing the accession id and pass downstream
identify_repos () {
    accession_id=$1
    
    # these name patterns are unique to their source repositories
    is_geo=`grep 'GSE' <<< ${accession_id} | wc -l`
    is_ae=`grep '[A-Z]-' <<< ${accession_id} | wc -l`
    
    if [ "${is_geo}" -eq 1 ]; then
        _REPOS='GEO'
        _GEO_PREFIX='GSE'
        _GEO_ID=${accession_id/GSE}
    fi
    
    if [ "${is_ae}" -eq 1 ]; then
        _REPOS='AE'
        _AE_PREFIX=$(echo $(cut -d'-' -f1,2 <<< ${accession_id})-)
        _AE_ID=$(cut -d'-' -f3 <<< ${accession_id})
    fi
}

# SCRIPT INFO
# use ArrayExpress id (numeric bit only) to obtain and cleanup raw data
main () {
    identify_repos ${_ACCESSION_ID}
     
    if [[ ${_REPOS} == 'AE' ]]; then
        echo "Accessing ArrayExpress"
        get_file_list_ae
        get_raw_data_ae
        extract_sample_info_ae
        inflate_raw_data_ae
    fi

    if [[ ${_REPOS} == 'GEO' ]]; then
        echo "Accessing GEO repository"
        get_sample_info_geo
        extract_sample_info_geo
        get_raw_data_geo
        inflate_raw_data_geo
    fi

    identify_data_format
    echo "Done"
}

# we expect 3 args, <directory_path> <accession_id> <data_repos_id>
ARGS=2

if [ $# != "$ARGS" ]; then
    echo "Usage: `basename $0` <directory> <accession id>"
    echo "Gets raw data and sample information from online repository given a dataset directory and accession id. Identifies format, extracts and attempts a cleanup of raw filenames. Dumps sample info to <directory>/samples.txt and sample phenotype information to <directory>/raw_sample_pheno.txt." | fmt -t
    exit 0
fi

main
