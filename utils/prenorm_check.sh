#!/bin/sh
# FIXME needs a rework
# run this validation check just before normalization
# chip id fields in dataset_dir/sample_pheno.txt == dataset_dir/source/raw/*.
# TODO check biosamples_metadata.txt as well
__author__="Tyrone Chen"
__copyright__=""
__credits__=["Othmar Korn, Rowland Mosbergen, Tyrone Chen"]
__license__="GPL"
__version__="0.0.2-dev"
__maintainer__="Tyrone Chen"
__email__="t.chen@uq.edu.au"
__status__="Prototype"

if [[ $# -eq 0 ]] ; then
    echo "usage: $0 <directory>"
    exit 0
fi

load_arrays() {
# get a list of files with extension .xkcd, get basenames, load array.
    _i=0

    for file in $datadir_path/*$1; do
        fname=$(echo ${file##*/})
        _FNAMES[ $_i ]=$(echo ${fname%$1})
        (( _i++ )) #FIXME i dont think this is POSIX compliant
    done
}

# infile paths
testdir_path=$1
datadir_path=$testdir_path/source/raw

_logfile0=$testdir_path/_sample_pheno_chip_ids.txt
_logfile1=$testdir_path/_fname_chip_ids.txt
logfile2=$testdir_path/log_chip_ids.txt

#TODO remove this bit later, not really necessary, another block covers this
# files in dataset_dir/source/raw should be either in .CEL or .txt format
cel_state=$(ls $datadir_path | grep .CEL. | wc -l)
txt_state=$(ls $datadir_path | grep .txt. | wc -l)

if [ $cel_state -eq 0 -a $txt_state -eq 0 ]; then
    echo "No compressed files detected in $datadir_path"
else
    echo "Files should be in .CEL or .txt format only"
    ls $datadir_path
    exit
fi

# get 1st column of sample_pheno.txt
sample_pheno="$testdir_path/sample_pheno.txt"
chip_ids=( $(cut -f1 $sample_pheno) )

# glob all filenames in source/raw, strip .CEL suffix
shopt -s nullglob

cel_files=($datadir_path/*.CEL)
txt_files=($datadir_path/*.txt)

# files in dataset_dir/source/raw should be either in .CEL or .txt format
if [ ${#cel_files[@]} -eq 0 -a ${#txt_files[@]} -eq 0 ]; then
    echo "No .CEL or .txt files in $datadir_path!"
    exit
fi

# load .CELs if present
if [ ${#cel_files[@]} -eq 0 ]; then
    echo "No .CEL files in $datadir_path"
else
    load_arrays .CEL
fi

# load .txt if present
if [ ${#txt_files[@]} -eq 0 ]; then
    echo "No .txt files in $datadir_path"
else
    load_arrays ".txt"
fi

# sort the arrays for comparison
readarray -t chip_ids < <(for x in "${chip_ids[@]}"; do echo "$x"; done | sort)
readarray -t fnames < <(for x in "${_FNAMES[@]}"; do echo "$x"; done | sort)

# compare chip ids with filenames
if [ "${chip_ids[*]}" == "${_FNAMES[*]}" ]; then
    echo "Chip IDs in sample_pheno.txt match input files"
else
    difference=()
    for i in "${chip_ids[@]}"; do
        skip=
        for j in "${_FNAMES[@]}"; do
            [[ $i == $j ]] && { skip=1; break; }
        done
        [[ -n $skip ]] || difference+=("$i")
    done
    
    # shows where it went wrong
    echo "MISMATCH" ${difference[*]}
    echo "Chip IDs in sample_pheno.txt must match input files"
    
    # crude but works
    touch $_logfile0; echo "sample_pheno" >> $_logfile0
    for i in ${chip_ids[*]}; do echo $i >> $_logfile0; done
    touch $_logfile1; echo "raw_fnames" >> $_logfile1
    for i in ${_FNAMES[*]}; do echo $i >> $_logfile1; done
    paste $_logfile0 $_logfile1 > $logfile2
    echo "Details present in" $logfile2
    
    # clears intermediate files
    rm $_logfile0; rm $_logfile1
fi
exit
