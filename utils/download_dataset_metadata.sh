#!/bin/bash
# grab metadata after init with chip id (to be used in dataset root)
wget "http://stemformatics.aibn.uq.edu.au/agile_org/files/dataset/${PWD##*/}/METASTORE" -O METASTORE
wget "http://stemformatics.aibn.uq.edu.au/agile_org/files/dataset/${PWD##*/}/biosamples_metadata.txt" -O biosamples_metadata.txt

